def get_fibonacci_value(position: int, first_value: int,
                        second_value: int) -> int:
    """
    :param position:  Position in fibonacci sequence
    :param first_value: First value of fibonacci sequence
    :param second_value: Second value of fibonacci sequence
    :return: value of fibonacci sequence on given position
    """
