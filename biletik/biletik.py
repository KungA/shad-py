def get_nearest_happy_ticket(current_ticket: str) -> str:
    """
    :param current_ticket: ticket number with 6 digits
    :return: nearest happy ticket number with 6 digits
    """
