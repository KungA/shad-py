def get_value(value: int, repeats: int) -> int:
    """
    :param value: value to repeat
    :param repeats: number of repeats
    :return: square of repeated value
    """
