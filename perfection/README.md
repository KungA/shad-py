# Совершенство

Совершенное число — это натуральное число, равное сумме всех своих собственных делителей.
В этой задаче Вася предлагает вам познать совершенство.

Ваша задача реализовать функцию `is_perfect`, которая для натурального числа определяет,
является ли оно совершенным

### Пример
```
>>> is_perfect(6)
True
```

### Примечание
Математические функции можно найти в модуле [`math`](https://docs.python.org/3/library/math.html).
