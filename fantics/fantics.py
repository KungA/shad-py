from typing import Set


def get_combinations(fantics_num: int, coins_nominals: Set[int]) -> int:
    """
    :param fantics_num: number of fantics to split on coins
    :param coins_nominals: values of coins
    :return: number of combinations to split fantics by coins
    """
