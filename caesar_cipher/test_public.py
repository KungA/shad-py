from caesar_cipher import make_caesar


def test_example():
    input = 'This is stupid song stupid stupid stupid song'
    output = 'Drsc sc cdezsn cyxq cdezsn cdezsn cdezsn cyxq'
    assert make_caesar(input, 10) == output
