from typing import List, TypeVar

FizzBuzzType = TypeVar('FizzBuzzType', int, str)


def get_fizz_buzz(n: int) -> List[FizzBuzzType]:
    """
    :param n: size of sequence
    :return: list of values. If value divided by 3 - "Fizz",
    if value divided by 5 - "Buzz", if value divided by 15 -
            "Fizz Buzz", else - value.
    """
